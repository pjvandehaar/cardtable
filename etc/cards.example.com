map $http_upgrade $connection_upgrade {
  default upgrade;
  '' close;
}
server {
  server_name cards.example.com;

  location / {
    include proxy_params;
    proxy_pass http://localhost:8801;
  }
  location /socket.io/ {
    # from https://stackoverflow.com/a/29232687/1166306
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Host $http_host;
    proxy_set_header X-NginX-Proxy false;

    proxy_pass http://localhost:8801;
    proxy_redirect off;

    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    # proxy_set_header Connection "upgrade";
    proxy_set_header Connection $connection_upgrade; # mentioned a few places

    proxy_ssl_session_reuse off; # added by https://stackoverflow.com/a/45456429/1166306
  }

  listen 443 ssl; # managed by Certbot
  ssl_certificate /etc/letsencrypt/live/example.com/fullchain.pem; # managed by Certbot
  ssl_certificate_key /etc/letsencrypt/live/example.com/privkey.pem; # managed by Certbot
  include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
  ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot
}
server {
  if ($host = cards.example.com) {
    return 301 https://$host$request_uri;
  } # managed by Certbot

  listen 80;

  server_name cards.example.com;
  return 404; # managed by Certbot
}
