# Todo:

- Fix dragging on iOS Safari:
  - On my iPhone SE, touchstart and touchend never happen, which can result in a card jumping to the next place touched.
  - Somehow we need to end the drag when the finger leaves the screen, or the next time it touches somewhere else.

- Save syncmap state to json when killed and load it at start

- Show Mac/Linux/Windows/iOS/Android in the server logs (and on the svg or admin panel?)

- Clicking on a name triggers collectTrickN() (via their cards and a circlular click-catcher behind their name)

- Use a server-centric data-flow:
  - The server is the primary authority on state.
  - The client has a `syncmap` object that follows the server, and overlays predictions of future server state based on sent modifications.
    - `syncmap` exposes `.get(key, path)`, `.set(key, path, value)`, and `.onChange(key => {})`.
  - Vue's state follows the syncmap, via `syncmap.onChange(key => { Vue.set(...) })`.
  - Under this model, consider using key=`card:${cardID}`, so that all data can be held in a single syncmap.

- Flatten the svg into a single Vue Component, or at least combine table_view with cardtable.
- Render a single list of all cards, and have each card choose its own location and size (without any whole-table <g transform=rotate>)
  - perhaps use `computed: playersHands() { return {self: [], others: {<name>:[]}}}`

- Add a wiki textbox for Euchre score, bids, trump chosen (or embed etherpad-lite?)
  - add a 3rd syncmap named "etc" for config (card size?) and textbox data.

- Require a password: `getJsonFromUrl().password.toLowerCase().replace(/\s/g,'') === 'vandehaar'` (and cache its input on index.html)
- When a new state comes in, animate moves over 100ms.

- Play a sound when picking up, dropping, or flipping a card.

- Make syncmap more robust
  - Only touch `cardtable.cards/users` in `cardtable.syncmap_modify()` which takes a obj or [obj].
