#!/usr/bin/env python3

import csv, itertools, collections, re, tqdm, json, sys, msgpack, brotli
from itertools import islice
from kpa.func_utils import assign, dict_from_iter
from kpa.func_cache_utils import shelve_cache
from sortedcontainers import SortedDict

from IPython.core import ultratb; sys.excepthook = ultratb.FormattedTB(mode='Verbose', color_scheme='Linux', call_pdb=1)



ipv4_fpath = 'GeoLite2-City-Blocks-IPv4.csv'
iso_codes_fpath = 'GeoLite2-City-Locations-en.csv'

cidr_regex = re.compile(r'^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})/(\d+)$')
def range_from_cidr(cidr):
    m = cidr_regex.match(cidr)
    if not m: return None
    octets = [int(m.group(idx)) for idx in [1,2,3,4]]
    start = octets[0]*256**3 + octets[1]*256**2 + octets[2]*256 + octets[3]
    num_bits = 32 - int(m.group(5))
    num_ips = 2 ** num_bits
    return start, num_ips
assert range_from_cidr('127.0.0.1/24') == (127*256**3+1, 256)

@assign
@shelve_cache
@dict_from_iter
def name_for_geoID():
    for line in tqdm.tqdm(csv.DictReader(open(iso_codes_fpath))):
        geoID = int(line['geoname_id'])
        if line['country_iso_code'] == 'US' and line['subdivision_1_iso_code'] in 'MI IA' and line['city_name']:
            name = line['subdivision_1_iso_code'] + ':' + line['city_name']
        elif line['country_iso_code'] == 'US' and line['subdivision_1_iso_code']:
            name = line['subdivision_1_iso_code']
        elif line['country_name']:
            name = line['country_name']
        elif line['continent_name']:
            name = line['continent_name']
        else:
            raise Exception(line)
        yield geoID, name


@assign
@shelve_cache
def x():
    x = SortedDict()
    for line in tqdm.tqdm(csv.DictReader(open(ipv4_fpath))):
        name = name_for_geoID[int(line['geoname_id'])] if line['geoname_id'] else '?'
        ip, length = range_from_cidr(line['network'])

        # assert that we aren't already in the list
        assert ip not in x, ip

        matches_smaller = matches_bigger = False
        idx_of_bigger_ip = x.bisect(ip)
        if idx_of_bigger_ip < len(x):
            bigger_ip = x.peekitem(idx_of_bigger_ip)[0]
            if ip + length == bigger_ip:
                if x[bigger_ip]['name'] == name:
                    matches_bigger = True
        idx_of_smaller_ip = idx_of_bigger_ip - 1
        if idx_of_smaller_ip >= 0:
            smaller_ip = x.peekitem(idx_of_smaller_ip)[0]
            if smaller_ip + x[smaller_ip]['length'] == ip:
                if x[smaller_ip]['name'] == name:
                    matches_smaller = True

        if matches_bigger:
            length += x[bigger_ip]['length']
            del x[bigger_ip]

        if matches_smaller:
            x[smaller_ip]['length'] += length
        else:
            x[ip] = {'length':length, 'name':name}
    return x


# TODO: name = country_name for every interval w/ length < 64,  re-merge
#       name = continent_name for every interval w/ length < 64,  re-merge

# TODO: insert blank intervals in empty ranges, and then filter out length<64 again

for ip in list(x.keys()):
    if x[ip]['length'] < 64 and not x[ip]['name'].startswith(('MI','IA')):
        del x[ip]

# # How many of each length
# for min_length in [1,2,4,8,16,32,64,128,256,512,1024]:
#     num = sum(1 for v in x.values() if v['length'] >= min_length)
#     perc = (sum(
#         v['length'] for v in x.values() if v['name'] and v['length'] >= min_length
#     ) / 256**4)
#     print(f'{int(min_length):7}  {num:6}  {perc*100:.2f}%')

# # How many of each length within states I care about
# for min_length in [1,2,4,8,16,32,64,128,256,512,1024]:
#     num = sum(1 for v in x.values() if v['length'] >= min_length and v['name'].startswith('MI:'))
#     perc = (sum(
#         v['length'] for v in x.values() if v['name'].startswith('MI:') and v['length'] >= min_length
#     ) / 256**4)
#     print(f'{int(min_length):7}  {num:6}  {perc*100:.2f}%')


# # How many distinct names
# print(len(set(v['name'] for k,v in x.items())), 'distinct names')

# # Try some IPs
# def lookup(ip):
#     idx_after_ip = x.bisect_right(range_from_cidr(ip+'/0')[0])
#     return x.peekitem(idx_after_ip - 1)
# for ip in [
#         '64.53.223.97', # dad - Mason
#         '73.145.4.147', # H - GR
#         '107.195.80.213', # brinks? - GR
#         '173.225.200.132', # mark - BR
#         '38.142.65.18', # spider?
#         '66.249.72.32', # spider
#         '66.249.72.32', # spider
#         '66.249.73.227', # spider
#         '172.58.139.44', # spider
#         '199.116.115.131', # spider
# ]:
#     print(ip, lookup(ip))

# # Show the most common countries for each Class A Block
# # (When an interval crosses Class A Block boundaries, we get odd results)
# # Compare to https://ant.isi.edu/address/it.37.all.16-subnet_stats.3px-per-point.annotated.png
# # Compare to https://xkcd.com/195/
# def counter_from_pairs(pairs):
#     c = collections.Counter()
#     for k,v in pairs: c[k] += v
#     return c
# for i in range(225):
#     col = counter_from_pairs((v['name'],v['length']) for k,v in x.items() if i*256**3 <= k < (i+1)*256**3)
#     print(f'{i:3} {sum(col.values())/256**3*100:3.0f}%', {k:round(v/256**3*100) for k,v in col.most_common(4)})


intervals = []
for k,v in x.items():
    if not intervals or intervals[-1]['name'] != v['name']:
        intervals.append({'ip':k, 'name':v['name']})

namelist = [k for k,v in collections.Counter(intv['name'] for intv in intervals).most_common()]
idx_for_name = {name:idx for idx,name in enumerate(namelist)}

ipv4offset_nameidx_pairs = []
for i in range(len(intervals)):
    ipv4offset_nameidx_pairs.append(intervals[i]['ip'] if i==0 else (intervals[i]['ip'] - intervals[i-1]['ip']))
    ipv4offset_nameidx_pairs.append(idx_for_name[intervals[i]['name']])
    # ipv4offset_nameidx_pairs.append((intervals[i]['ip'] if i==0 else (intervals[i]['ip'] - intervals[i-1]['ip']),
    #                                 idx_for_name[intervals[i]['name']]))

obj = {
    'ipv4offset_nameidx_pairs': ipv4offset_nameidx_pairs,
    'namelist': namelist,
}
#with open('intervals.json', 'wb') as f: json.dump(obj, f, separators=(',',':'))
with open('intervals.json.br', 'wb') as f: f.write(brotli.compress(json.dumps(obj, separators=(',',':')).encode('utf8')))
