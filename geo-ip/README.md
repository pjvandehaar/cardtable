## Generating the intervals file:

Download the MaxMind database:

- Make an account, get a license key, wait 5 minutes.
- `wget -O geoip.zip https://download.maxmind.com/app/geoip_download?edition_id=GeoLite2-City-CSV&license_key=${LICENSE_KEY}&suffix=zip`
- `unzip geoip.zip`
- put `*-en.csv` and `*-IPv4.csv` in this directory.

Run `./make_geoip_intervals_file.py`.


## Class A Blocks:
- 0: local
- 10: VPN
- 127: loopback
- 224-239: multicast
- 240-255: reserved
