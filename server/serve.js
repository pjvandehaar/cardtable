#!/usr/bin/env nodemon
/* -*- js-indent-level:2; -*- */

// Consider rebuilding location_from_ipv4 with sqlite3 to save RAM


// // Sentry doesn't seem to be working.  The Express integrations didn't either.
// const Sentry = require('@sentry/node');
// Sentry.init({ dsn: 'https://fec6e0f2eabc4a3e982eb6cc21da12d0@o390476.ingest.sentry.io/5236849' });


const app = require('express')();
const http = require('http').createServer(app);
const io = require('socket.io')(http);
const _ = require('lodash');
const fs = require('fs');
const choose_rotation = require('./utils').choose_rotation;
const location_from_ipv4 = require('./location_from_ipv4');

const stateFilePath = 'state.json';


app.get('/', (req, res) => {
  res.sendFile('index.html', {root:'../client/'});
});
app.get('/robots.txt', (req, res) => {
  res.setHeader('content-type', 'text/plain');
  res.send('User-agent: *\nDisallow: /\n');
});
app.get('/table.html', (req, res) => {
  res.sendFile('table.html', {root:'../client/'});
});
app.get('/table.js', (req, res) => {
  res.sendFile('table.js', {root:'../client/'});
});
app.get('/ip', (req, res) => {
  res.setHeader('content-type', 'text/plain');
  const client_ip = req.headers['x-real-ip'] || req.connection.remoteAddress;
  res.send(
    `IP = ${client_ip}\n` +
      `Geo = ${location_from_ipv4.lookup(client_ip)}\n` +
      `Headers = ${JSON.stringify(req.headers)}\n`
  );
});


const users = {};
const cards = {
  0: {label: 'Ad',    pos: {type:'table', x: 0.4, y:0.6, z:0, rot:0.3, faceup:false}},
  1: {label: '1^Bat', pos: {type:'table', x: 0.5, y:0.3, z:1, rot:0.1, faceup:true}},
  2: {label: '10h',   pos: {type:'table', x: 0.4, y:0.6, z:3, rot:0,   faceup:true}},
  3: {label: 'Kc',    pos: {type:'table', x: 0.8, y:0.5, z:4, rot:0.4, faceup:false}},
  4: {label: 'Je',    pos: {type:'table', x: 0.3, y:0.6, z:2, rot:0.4, faceup:true}},
  5: {label: 'As',    pos: {type:'table', x: 0.3, y:0.6, z:2, rot:0.7, faceup:true}},
};
if (fs.existsSync(stateFilePath)) {
  // TODO: does this need try-catch?
  const stateRawData = fs.readFileSync(stateFilePath);
  const stateData = JSON.parse(stateRawData);
  if (_.isObject(stateData) && _.isObject(stateData.cards)) {
    Object.keys(cards).forEach( cardID => { delete cards[cardID]; });
    Object.assign(cards, stateData.cards);
  }
}

let next_socket_id = 1;
io.on('connection', (socket) => {
  const my_socket_id = next_socket_id++;
  let my_name = null;
  const my_ip = socket.handshake.headers['x-real-ip'] ? socket.handshake.headers['x-real-ip'] : socket.request.connection.remoteAddress;
  const my_location = location_from_ipv4.lookup(my_ip);
  function preamble() {
    const padded_name = my_name===null ? ''.padStart(8) : typeof my_name==='string' ? my_name.padEnd(8).substr(0,8) : ''.padStart(8,'-');
    const padded_ip = my_ip===null ? ''.padStart(15) : typeof my_ip==='string' ? my_ip.padEnd(15).substr(0,15) : ''.padStart(15,'-');
    const padded_location = my_location===null ? ''.padStart(20) : typeof my_location==='string' ? my_location.padEnd(20).substr(0,20) : ''.padStart(20,'-');
    return `#${my_socket_id} (${padded_name}@${padded_ip}/${padded_location})`;
  }
  console.log(preamble() + ` connected from ${my_ip}`);

  socket.on('disconnect', () => {
    console.log(preamble() + ` disconnected`);
    if (_.has(users, my_name) && _.has(users[my_name], 'locations') &&_.has(users[my_name].locations, my_location)) {
      users[my_name].locations[my_location]--;
      if (users[my_name].locations[my_location] === 0) {
        delete users[my_name].locations[my_location];
      }
    }
  });

  socket.on('users:join', (name) => {
    my_name = name;

    if (!_.has(users, my_name)) {
      users[my_name] = { rot:choose_rotation(users) };
    }
    if (!_.has(users[my_name], 'locations')) { users[my_name].locations = {}; }
    users[my_name].locations[my_location] = 1 + (users[my_name].locations[my_location] || 0);
    console.log(preamble() + ` joined`);
    io.emit('syncmap:full_update', 'users', users);
    console.log(`** broadcasted users[${Object.keys(users).length}] ≈`, Object.keys(users));
    socket.emit('syncmap:full_update', 'cards', cards);
  });
  socket.on('syncmap:modify', (syncmap_name, modifications) => {
    console.log(preamble() + ` modifies ${syncmap_name}:`, modifications);
    const syncmap = {users: users, cards:cards}[syncmap_name];
    for (let mod of modifications) {
      if (mod.type === 'delete') {
        delete syncmap[mod.key];
      } else if (mod.type === 'set') {
        syncmap[mod.key] = mod.value;
      } else if (mod.type === 'update') {
        if (syncmap[mod.key]) {
          for (let key in mod.updates) {
            syncmap[mod.key][key] = mod.updates[key];
          }
        }
      }
    }
    // track which keys were modified and do socket.broadcast.emit() of just those
    const modified_keys = modifications.map(mod => mod.key);
    const partial_update_map = {};
    for (let modified_key of modified_keys) {
      if (_.has(syncmap, modified_key)) {
        partial_update_map[modified_key] = syncmap[modified_key];
      } else {
        partial_update_map[modified_key] = null;
      }
    }
    socket.broadcast.emit('syncmap:partial_update', syncmap_name, partial_update_map);
    console.log(`** broadcasted partial_update of ${syncmap_name}[${Object.keys(cards).length}] to all sockets except #${my_socket_id}: ${partial_update_map}`);
  });

  socket.on('request_for_update', () => {
    console.log(preamble() + ` requested update`);
    socket.emit('syncmap:full_update', 'users', users);
    socket.emit('syncmap:full_update', 'cards', cards);
  });
});



const argv = require('yargs').argv;
const port = argv.port || 5000;
http.listen(port, () => {
  console.log(`listening on *:${port}`);
});


function handleExit(options) {
  console.log(`handleExit() triggered by ${options.eventType} with ${Object.keys(cards).length} cards`);
  try {
    // Note: if this fails due to permissions, try `touch server/state.json && chmod a+rw server/state.json`
    fs.writeFileSync(stateFilePath, JSON.stringify({cards:cards}));
  } catch(err) {
    console.log(`handleExit() failed to save state due to ${err}`);
  }
  if (!options.stayAlive) {
    // This signal handler replaces the original one which would have exited, so we must exit here.
    process.exit();
  }
}
const exitEvents = [
  'SIGTERM', // at system shutdown
  'SIGINT', // ctrl-c
  'SIGUSR1', 'SIGUSR2', // `kill $pid`, or `nodemon` restarting
  'uncaughtException',
];
exitEvents.forEach(eventType => {
  process.on(eventType, handleExit.bind(null, {eventType:eventType}));
});
