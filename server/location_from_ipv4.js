#!/usr/bin/env nodemon

const _ = require('lodash');
const fs = require('fs');
const brotli_decompress = require('brotli/decompress');

const intervals = (function() {
    // Somehow this function causes permanent RAM usage that's not recovered by GC (experiment while watching `node --trace_gc serve.js`)
    const x_decompressed = brotli_decompress(fs.readFileSync('../geo-ip/intervals.json.br'));
    const x_json = new Buffer.from(x_decompressed).toString();
    const x = JSON.parse(x_json);

    const intervals = [];
    for (let idx=0; idx < x.ipv4offset_nameidx_pairs.length; idx+=2) {
        intervals.push({
            ip: x.ipv4offset_nameidx_pairs[idx] + (idx===0 ? 0 : intervals[intervals.length-1]['ip']),
            name: x.namelist[x.ipv4offset_nameidx_pairs[idx+1]],
        });
    }
    return intervals;
})();
// console.log(intervals.slice(0,4), intervals.length);

function lookup(ipv4) {
    if (typeof ipv4 === 'string') {
        const m = /^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$/.exec(ipv4);
        if (m && m[1]) { ipv4 = (+m[1]*256*256*256) + (+m[2]*256*256) + (+m[3]*256) + (+m[4]); }
    }
    if (typeof ipv4 !== 'number') { return ''; }
    const idx = _.sortedLastIndexBy(intervals, {ip:ipv4}, _.property('ip')) - 1;
    if (idx < 0) return '';
    return intervals[idx]['name'];
}
// console.log(lookup_location(16778240));
// console.log(lookup_location(16778241));
// console.log(lookup_location('173.225.200.132'));
// console.log(lookup_location(2917255296));

module.exports = {
    lookup
};
