const _ = require('lodash');

function choose_rotation(users) {
  if (Object.keys(users).length === 0) {
    return 0;
  } else if (Object.keys(users).length === 1) {
    return (users[Object.keys(users)[0]].rot + 0.5) % 1;
  } else {
    const rots = _.sortBy(Object.values(users).map(u => u.rot));
    const gaps = _.range(rots.length-1).map(idx => ({
      smaller: rots[idx], bigger: rots[idx+1]
    }));
    gaps.push({ smaller: rots[rots.length-1], bigger:rots[0]+1 });
    for (let gap of gaps) { gap.size = gap.bigger - gap.smaller; }
    const biggest_gap = _.maxBy(gaps, _.property('size'));
    return ((biggest_gap.bigger + biggest_gap.smaller) / 2) % 1;
  }
}

module.exports = {
    choose_rotation
};
