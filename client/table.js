/* -*- js-indent-level:2; -*- */

/* global _, Vue, VTooltip, io */

const normal_card_regex = /^([2-9JQKA]|10)([hcds])$/;
const rank_order = ["A","K","Q","J","10","9","8","7","6","5","4","3","2"];
const suit_order = 'hcds';
function card_label_sort_key(label) {
  // first, the normal 52: Ah, Kh, Qh, ... 2h, ... 2s (which get -100 to -47)
  // then, things that start with numbers, which get 0 to 1.
  // then, anything else, which is in 1 to 2.
  // Note: it would have been way better to just make a [Num, Str] tuple.
  if (label === '') { return -1e3; }
  const match = normal_card_regex.exec(label);
  if (match) {
    const rank = match[1], suit = match[2];
    return rank_order.indexOf(rank) + suit_order.indexOf(suit) * 13 - 100;
  } else if (/^[0-9]/.test(label[0])) {
    const match2 = /^([0-9][0-9]?)(.*)$/.exec(label);
    return parseFloat(match2[1])/100 + string_to_sort_fraction(match2[2])/100;
  } else {
    return 1 + string_to_sort_fraction(label);
  }
}
function string_to_sort_fraction(s) {
  // converts every string to a number in 0-1, hopefully preserving sort order
  const num_unicode_points = Math.pow(2,21); // upperbound
  let ret = 0;
  Array.from(s).map(c => c.charCodeAt(0)).forEach((elem, idx) => {
    ret += elem / Math.pow(num_unicode_points, 1+idx);
  });
  return ret;
}
function quoteless_stringify(x) {
  if (typeof x === "number") { return Number.isInteger(x) ? x : x.toPrecision(4); }
  if (Array.isArray(x)) { return '[' + x.map(quoteless_stringify).join(', ') + ']'; }
  if (typeof x !== "object" || x === null) {
    return JSON.stringify(x);
  }
  let props = Object.keys(x)
    .map(key => `${key}:${quoteless_stringify(x[key])}`)
    .join(", ");
  return `{${props}}`;
}
function unicodified_card_label(label) {
  const suit_symbols = {h: '\u2665', c:'\u2663', d:'\u2666', s:'\u2660'};
  const match = normal_card_regex.exec(label);
  if (!match) { return label; }
  const rank = match[1], suit = match[2];
  // return rank + '\u200a' + suit_symbols[suit];
  return rank + suit_symbols[suit];
}
function setDefaults(obj, defaults) {
  for (let key in defaults) {
    if (!_.has(obj, key)) { obj[key] = defaults[key]; }
  }
}
function rotate_xy(oldX, oldY, turns, originX, originY) {
  if (typeof originX === 'undefined' && typeof originY === 'undefined') {
    originX = originY = .5;
  }
  // rotate `old` around `origin`
  const xOffset = oldX - originX;
  const yOffset = oldY - originY;
  const newXOffset = xOffset*Math.cos(2*Math.PI*turns) - yOffset*Math.sin(2*Math.PI*turns);
  const newYOffset = yOffset*Math.cos(2*Math.PI*turns) + xOffset*Math.sin(2*Math.PI*turns);
  return [newXOffset+originX, newYOffset+originY];
}
function getJsonFromUrl(url) {
  if(!url) {url = location.href;}
  var question = url.indexOf("?");
  var hash = url.indexOf("#");
  if(hash===-1 && question===-1) {return {};}
  if(hash===-1) {hash = url.length;}
  var query = question===-1 || hash===question+1 ? url.substring(hash) :
    url.substring(question+1,hash);
  var result = {};
  query.split("&").forEach(function(part) {
    if(!part) {return;}
    part = part.split("+").join(" "); // replace every + with space, regexp-free version
    var eq = part.indexOf("=");
    var key = eq>-1 ? part.substr(0,eq) : part;
    var val = eq>-1 ? decodeURIComponent(part.substr(eq+1)) : "";
    var from = key.indexOf("[");
    if(from===-1) {result[decodeURIComponent(key)] = val;}
    else {
      var to = key.indexOf("]",from);
      var index = decodeURIComponent(key.substring(from+1,to));
      key = decodeURIComponent(key.substring(0,from));
      if(!result[key]) {result[key] = [];}
      if(!index) {result[key].push(val);}
      else {result[key][index] = val;}
    }
  });
  return result;
}
const my_name = getJsonFromUrl().name || '<nameless>';


Vue.use(VTooltip);

Vue.component('cardtable', {
  template: '#cardtable-template',
  data: function() {
    return {
      users: {}, cards: {},
      my_name:my_name, table_view_width:600, manager_mode:false, admin_mode:false,
      messagelist: [], show_deal_buttons: false,
    };
  },
  created: function() {
    window.addEventListener('resize', this.windowResizeHandler);
    this.windowResizeHandler(); // resizing from nothing, I guess
  },
  destroyed: function() {
    window.removeEventListener('resize', this.windowResizeHandler);
  },
  methods: {
    windowResizeHandler: function() {
      const height_per_width = 1.15;
      const available_width = Math.min(
        window.innerWidth,
        document.querySelector('html').clientWidth) - 10;
      const avilable_height = Math.min(
        window.innerHeight,
        document.querySelector('html').clientHeight) - 10;
      if (available_width * height_per_width < avilable_height) {
        this.table_view_width = available_width;
      } else {
        this.table_view_width = avilable_height / height_per_width;
      }
    },
    // Event Handlers:
    postMessage: function(msg) {
      const d = new Date();
      this.messagelist.unshift(`${d.getHours()}:${d.getMinutes()}:${d.getSeconds()} `+msg);
    },
    kickUser: function(name) {
      // Move their cards out in front of where they were, face-down
      const rot = this.users[name].rot;
      const [destX, destY] = rotate_xy(.5,.8, -rot);
      _.each(this.cards, (v,cardID) => {
        if (v.pos.type === 'hand' && v.pos.user === name) {
          this.updateCardPos(cardID, {
            type:'table',
            x:destX + _.random(-.01, .01),
            y:destY + _.random(-.01, .01),
            z:this.biggestZ(),
            rot: -rot + _.random(-.05, .05),
            faceup:false
          });
        }
      });
      // Remove them, and tell the server we removed them
      Vue.delete(this.users, name);
      socket.emit('syncmap:modify', 'users', [{type:'delete', key:name}]);
    },
    onCardClicked: function(cardID, evt) {
      const pos = this.cards[cardID].pos;
      console.log('clicked', this.cards[cardID].label, JSON.stringify(pos), evt);
      const isRightClick = evt.button === 2;
      if (pos.type === "hand" && pos.user === this.my_name) {
        // TODO: if the destination is occupied, shift down a bit
        const my_rotation = this.users[my_name].rot;
        const [x, y] = [0.5, 0.6];
        const [newX, newY] = rotate_xy(x, y, -my_rotation);
        this.updateCardPos(cardID, {
          type:'table',
          x: newX + _.random(-.01, .01),
          y: newY + _.random(-.01, .01),
          z:this.biggestZ(),
          rot:-my_rotation + _.random(-.01, .01),
          faceup:!isRightClick});
      } else if (pos.type === "table") {
        if (isRightClick) {
          this.updateCardPos(cardID, {faceup: !this.cards[cardID].pos.faceup})
        } else {
          //this.updateCardPos(cardID, {type:'hand', user:my_name, faceup:true});
        }
      } else if (pos.type === 'hand' && pos.user !== this.my_name) {
        if (this.admin_mode) {
          window.location.href = '/table.html?name=' + encodeURIComponent(pos.user);
        }
      }
    },
    playCard: function(cardID) {
      const my_rotation = this.users[my_name].rot;
      const [x, y] = [0.5, 0.6];
      const [newX, newY] = rotate_xy(x, y, -my_rotation);
      this.updateCardPos(cardID, {
        type:'table',
        x: newX + _.random(-.01, .01),
        y: newY + _.random(-.01, .01),
        z:this.biggestZ(),
        rot:-my_rotation + _.random(-.01, .01),
        faceup:true
      });
    },
    onCardChanged: function(cardID, evt) {
      const param = evt.target.name;
      let newPos = {};
      if (evt.target.tagName === "SELECT") {
        newPos[param] = _.find(evt.target.childNodes, n => n.selected).value;
        const defaults = {x:0.5, y:0.5, z:0, rot:0, faceup:true, user: Object.keys(this.users)[0]};
        for (let key in defaults) { if(!_.has(this.cards[cardID].pos, key)) { newPos[key] = defaults[key]; }}
      } else if (evt.target.tagName === "INPUT") {
        if (evt.target.type === "checkbox") {
          newPos[param] = evt.target.checked;
        } else {
          newPos[param] = +evt.target.value;
        }
      }
      //console.log('card changed:', cardID, JSON.stringify(newPos), evt);
      this.updateCardPos(cardID, newPos);
    },
    spawnCard: function(label) {
      this.addCard(label, {
        type:'table',
        x:Math.round(45+Math.random()*10)/100,
        y:Math.round(45+Math.random()*10)/100,
        z:this.biggestZ(),
        rot:_.random(-0.1, 0.1),
        faceup:true,
      });
    },
    // Methods that actually modify `this.cards`:
    updateCardPos: function(cardID, newPos) {
      if (this.cards[cardID] && this.cards[cardID].pos) {
        const pos = this.cards[cardID].pos;
        for (let key in newPos) { Vue.set(pos, key, newPos[key]); }
        socket.emit('syncmap:modify', 'cards', [{type:'update', key:cardID, updates:{pos:this.cards[cardID].pos}}]);
      }
    },
    deleteCard: function(cardID) {
      Vue.delete(this.cards, cardID);
      socket.emit('syncmap:modify', 'cards', [{type:'delete', key:cardID}]);
    },
    addCard: function(label, pos) {
      const cardID = this.newCardID();
      setDefaults(pos, {type:'table', x:0.5, y:0.5, z:0, rot:0, faceup:true, user: Object.keys(this.users)[0]});
      const cardData = {label: label, pos:pos};
      Vue.set(this.cards, cardID, cardData);
      socket.emit('syncmap:modify', 'cards', [{type:'set', key:cardID, value:cardData}]);
    },
    deal: function(type) {
      this.show_deal_buttons = false;
      if (type === 'clear') { window.clearCards(); }
      else if (type === 'Euchre') { window.dealEuchre(); }
      else if (type === 'Pitch') { window.dealPitch(); }
      else if (type === 'CanadianSalad') { window.dealCanadianSalad(); }
      else if (type === 'Batman') { window.dealBatman(); }
      else if (/^Golf[0-9]+$/.test(type)) { window.dealGolf(+(/^Golf([0-9]+)/.exec(type)[1])); }
      else if (/^Wizard[0-9]+$/.test(type)) { window.dealWizard(+(/^Wizard([0-9]+)/.exec(type)[1])); }
      else if (/^OhHell[0-9]+$/.test(type)) { window.dealOhHell(+(/^OhHell([0-9]+)/.exec(type)[1])); }
    },
    updateUserRot: function(name, newRot) {
      newRot = Math.round(((newRot + 100) % 1) * 100) / 100;
      Vue.set(this.users[name], 'rot', newRot);
      socket.emit('syncmap:modify', 'users', [{type:'update', key:name, updates:{rot:newRot}}]);
    },

    // Helpers:
    newCardID: function() {
      let cardID;
      do { cardID = Math.floor(Math.random() * 1e6).toString(16); } while (_.has(this.cards, cardID));
      return cardID;
    },
    biggestZ: function(exclude_1e3) {
      let z = 0;
      for (let cardID in this.cards) {
        if (this.cards[cardID].pos.type === "table") {
          if (!exclude_1e3 || this.cards[cardID].pos.z < 1e3) {
            z = Math.max(z, this.cards[cardID].pos.z);
          }
        }
      }
      return z + 1;
    },
  },
  computed: {
    own_rotation: function() {
      return this.users[this.my_name] ? this.users[this.my_name].rot : 0;
    },
    sorted_cards: function() {
      const cardlist = _.map(this.cards, (v,k)=>({cardID:k,label:v.label,pos:v.pos}));
      return _.sortBy(cardlist, c => card_label_sort_key(c.label));
    },
  },
  components: {
    cardline: {
      props: { cardID:String, label:String, pos:Object, users:Object },
      template: '#cardline-template',
      computed: {
        displayed_label: function() { return unicodified_card_label(this.label); },
        quoteless_pos_json: function() { return quoteless_stringify(this.pos); },
      },
    },
    table_view: {
      props: { width:Number, users:Object, cards:Object, ownRotation:Number, my_name:String },
      template: '#table_view-template',
      methods: {
        cardlist_for: function(name) {
          let ret = [];
          _.each(this.cards, (v,cardID) => {
            if (v.pos.type==='hand' && v.pos.user===name) {
              ret.push({cardID:cardID, label:v.label, pos:v.pos});
            }
          });
          return ret;
        },
      },
      computed: {
        ordered_table_cards: function() {
          return _.sortBy(
            _.toPairs(this.cards).filter(([cardID, data]) => data.pos.type==="table").map(x => [x[1],x[0]]),
            c => c[0].pos.z);
        },
        own_held_cards: function() {
          const my_cards = _.toPairs(this.cards)
            .filter(([cardID, cardData]) => cardData.pos.type === "hand" && cardData.pos.user === this.my_name)
            .map(([cardID, cardData]) => ({cardID:cardID, cardData:cardData}));
          return _.sortBy(my_cards, c => card_label_sort_key(c.cardData.label));
        },
        own_locations: function() { 
          return Object.keys((this.users[this.my_name]||{}).locations||{}).join('; ');
        }
      },
      components: {
        table_user_view: {
          props: { relativeRotation:Number, name:String, cardlist:Array, locations:Array },
          template: '#table_user_view-template',
          computed: {
            transform: function() {
              // rot=0 is straight down
              const x = 0.5 + Math.sin(this.relativeRotation * 2*Math.PI) *.45;
              const y = 0.5 + Math.cos(this.relativeRotation * 2*Math.PI) *.45;
              return `translate(${x},${y})`;
            },
          },
        },
        own_hand: {
          props: { held_cards:Array },
          template: '#own_hand-template',
          computed: {
            card_width: function() {
              if (this.held_cards.length <= 6) { return 1/6; }
              return 1 / this.held_cards.length;
            },
          },
          methods: {
            transform: function(idx, card_width) {
              let x_widths_offset = idx + 0.5; // position by middle
              if (this.held_cards.length < 6) { x_widths_offset += (6-this.held_cards.length) / 2; }
              return `translate(${x_widths_offset*card_width},${card_width*1.5/2})`;
            }
          },
        },
      },
    },
  },
});

Vue.component('card', {
  props: { cardID:String, label:String, pos:Object, width:Number },
  data: function() {
    return { dragInfo:{is_dragging:false, startWindowXY:[null, null], startViewportXY:[null, null]} };
  },
  template: '#card-template',
  methods: {
    flip: function(evt) {
      console.log('flip', this.cardID);
      this.$emit('post-message', `flip ${this.cardID}`);
      this.$root.$children[0].updateCardPos(this.cardID, {
        faceup: !this.pos.faceup
      });
    },
    startDrag: function(evt) {
      if (evt.button !== 0) { return /* no right-click in this function */ }
      if (!(typeof evt.pageX === 'number' || evt.changedTouches && evt.changedTouches[0] && typeof evt.changedTouches[0].pageX === 'number')) {
        throw `Bad startDrag event: ${this.dragInfo} ${evt} ${evt.pageX} ${evt.changedTouches}`;
      }
      this.dragInfo.is_dragging = true;
      const evtX = (typeof evt.pageX === 'number') ? evt.pageX : evt.changedTouches[0].pageX;
      const evtY = (typeof evt.pageY === 'number') ? evt.pageY : evt.changedTouches[0].pageY;
      console.log(`startDrag ${evt.type} ${this.label} at ${evtX},${evtY}`, evt);
      this.$emit('post-message', `startDrag (${evt.type}) to ${this.label} at ${evtX},${evtY} with ${quoteless_stringify(evt)}`);
      // Attach listeners to `window` to catch them anywhere
      window.addEventListener('mousemove', this.extendDrag);
      // window.addEventListener('touchmove', this.extendDrag); // perhaps not needed but I wonder
      window.addEventListener('mouseup', this.endDrag);
      window.addEventListener('touchend', this.endDrag);
      document.querySelector('#app svg').addEventListener('mouseleave', this.endDrag);

      this.dragInfo.startWindowXY = [evtX, evtY];
      this.dragInfo.viewportXY = [this.pos.x, this.pos.y];
      this.$root.$children[0].updateCardPos(this.cardID, {
        z:this.$root.$children[0].biggestZ(),
        rot: -this.$root.$children[0].own_rotation
      });
    },
    extendDrag: function(evt) {
      if (!(typeof evt.pageX === 'number' || evt.changedTouches && evt.changedTouches[0] && typeof evt.changedTouches[0].pageX === 'number')) {
        throw `Bad extendDrag event: ${this.dragInfo} ${evt} ${evt.pageX} ${evt.changedTouches} ${evt.changedTouches[0]} ${evt.changedTouches[0].pageX}`;
      }
      const evtX = (typeof evt.pageX === 'number') ? evt.pageX : evt.changedTouches[0].pageX;
      const evtY = (typeof evt.pageY === 'number') ? evt.pageY : evt.changedTouches[0].pageY;
      if (!this.dragInfo.is_dragging) {
        // on iPhone, a drag triggers touchmove and then touchend without ever triggering touchstart
        this.dragInfo.is_dragging = true;
        this.$emit('post-message', `startless-extendDrag (${evt.type}) to ${this.label} at ${evtX},${evtY} with ${quoteless_stringify(evt)}`);
        window.addEventListener('touchend', this.endDrag);
        this.dragInfo.startWindowXY = [evtX, evtY];
        this.dragInfo.viewportXY = [this.pos.x, this.pos.y];
        this.$root.$children[0].updateCardPos(this.cardID, {
          rot: -this.$root.$children[0].own_rotation
        });
      }
      const deltaX = evtX - this.dragInfo.startWindowXY[0];
      const deltaY = evtY - this.dragInfo.startWindowXY[1];
      console.log(`extendDrag ${evt.type} ${this.label} at ${evtX},${evtY} (${deltaX},${deltaY})`, evt);
      this.$emit('post-message', `extendDrag (${evt.type}) to ${this.label} at ${evtX},${evtY} (${deltaX},${deltaY}) with ${quoteless_stringify(evt)}`);
      const rot = -this.$root.$children[0].own_rotation;

      if (this.pos.type === 'hand' && this.pos.user === this.$root.$children[0].my_name) {
        if (deltaY / document.querySelector('#app svg').clientHeight < -0.1) {
          // We've dragged out of the hand, so let's become a table card
          this.$root.$children[0].updateCardPos(this.cardID, {
            type:'table',
            x: evtX / document.querySelector('#app svg').clientWidth * 1,
            y: evtY / document.querySelector('#app svg').clientHeight * 1.15,
            rot: rot
          });
          this.dragInfo.startWindowXY = [0,0];
          this.dragInfo.viewportXY = rotate_xy(0,0, rot, 0.5,0.5);
        }
        return;
      }

      if (this.pos.type === 'table') {
        const [rotatedDeltaX, rotatedDeltaY] = rotate_xy(deltaX, deltaY, rot, 0,0);
        const viewportDeltaX = rotatedDeltaX / document.querySelector('#app svg').clientWidth * 1;
        const viewportDeltaY = rotatedDeltaY / document.querySelector('#app svg').clientHeight * 1.15;
        const viewportNewX = _.clamp(this.dragInfo.viewportXY[0] + viewportDeltaX, 0, 1);
        const viewportNewY = _.clamp(this.dragInfo.viewportXY[1] + viewportDeltaY, 0, 1);
        // console.log(
        //   '----' +
        //   `delta=${deltaX},${deltaY} `+
        //   `rot=${rot} ` +
        //   `rotDelta=${rotatedDeltaX},${rotatedDeltaY} `+
        //   `vpDelta=${viewportDeltaX},${viewportDeltaY} `+
        //   `vpNew=${viewportNewX},${viewportNewY}`);

        this.$root.$children[0].updateCardPos(this.cardID, {
          x:viewportNewX,
          y:viewportNewY,
        });
      }
    },
    endDrag: function(evt) {
      if (!this.dragInfo.is_dragging) { return; }
      this.dragInfo.is_dragging = false;
      if (!(typeof evt.pageX === 'number' || evt.changedTouches && evt.changedTouches[0] && typeof evt.changedTouches[0].pageX === 'number')) {
        throw `Bad endDrag event: ${this.dragInfo} ${evt} ${evt.pageX} ${evt.changedTouches} ${evt.changedTouches[0]} ${evt.changedTouches[0].pageX}`;
      }
      const evtX = (typeof evt.pageX === 'number') ? evt.pageX : evt.changedTouches[0].pageX;
      const evtY = (typeof evt.pageY === 'number') ? evt.pageY : evt.changedTouches[0].pageY;
      const deltaX = evtX - this.dragInfo.startWindowXY[0];
      const deltaY = evtY - this.dragInfo.startWindowXY[1];
      console.log(`endDrag ${evt.type} ${this.label} at ${evtX},${evtY} (${deltaX},${deltaY})`, evt);
      this.$emit('post-message', `endDrag (${evt.type}) to ${this.label} at ${evtX},${evtY} (${deltaX},${deltaY}) with ${quoteless_stringify(evt)}`);
      window.removeEventListener('mousemove', this.extendDrag);
      window.removeEventListener('mouseup', this.endDrag);
      window.removeEventListener('touchend', this.endDrag);
      document.querySelector('#app svg').removeEventListener('mouseleave', this.endDrag);

      const rot = -this.$root.$children[0].own_rotation;
      const [rotatedDeltaX, rotatedDeltaY] = rotate_xy(deltaX, deltaY, rot, 0,0);
      const viewportDeltaX = rotatedDeltaX / document.querySelector('#app svg').clientWidth * 1;
      const viewportDeltaY = rotatedDeltaY / document.querySelector('#app svg').clientHeight * 1.15;
      const viewportNewX = _.clamp(this.dragInfo.viewportXY[0] + viewportDeltaX, 0, 1);
      const viewportNewY = _.clamp(this.dragInfo.viewportXY[1] + viewportDeltaY, 0, 1);
      // console.log(
      //   '------' +
      //   `delta=${deltaX},${deltaY} `+
      //   `rot=${rot} ` +
      //   `rotDelta=${rotatedDeltaX},${rotatedDeltaY} `+
      //   `vpDelta=${viewportDeltaX},${viewportDeltaY} `+
      //   `vpNew=${viewportNewX},${viewportNewY}`
      // );
      
      if (this.pos.type === "hand" && this.pos.user === this.$root.$children[0].my_name) {
        console.log('in-hand', viewportDeltaX, viewportDeltaY);
        if (Math.abs(viewportDeltaX) < 0.05 && Math.abs(viewportDeltaY) < 0.05) {
          // Click-to-play
          console.log('clicktoplay');
          this.$emit('play-card', this.cardID);
        }
      } else if (this.pos.type === 'table') {
        if ((this.dragInfo.startWindowXY[1] + deltaY) / document.querySelector('#app svg').clientHeight * 1.15 < 0.93) {
          // Dragged on table
          console.log('drag-on-table');
          this.$root.$children[0].updateCardPos(this.cardID, {
            x:viewportNewX,
            y:viewportNewY,
            z:this.$root.$children[0].biggestZ()
          });
        } else {
          // Dragged into own_hand
          console.log('drag-into-hand');
          this.$root.$children[0].updateCardPos(this.cardID, {
            type:'hand',
            user:this.$root.$children[0].my_name,
            faceup:true,
          });
        }
      }
    },
  },
  computed: {
    height: function() { return this.width * 1.5; },
    transform: function() {
      // rot=0 is straight down
      if (this.pos.type === "table") {
        return `translate(${this.pos.x},${this.pos.y}) rotate(${360*this.pos.rot})`;
      } else {
        return '';
      }
    },
    displayed_label: function() { return unicodified_card_label(this.label); },
    text_color: function() {
      if (this.label.endsWith('h') || this.label.endsWith('d')) { return "red"; }
      return "black";
    },
    displayed_label_lines: function() {
      return this.displayed_label.split('^');
    },
  }
});

const app = new Vue({ el: '#app' });

/* exported cardtable */
const cardtable = app.$children[0];

var socket = io();
socket.emit('users:join', my_name);
socket.on('reconnect', () => {
  socket.emit('users:join', my_name); // this triggers a full_update
});

socket.on('syncmap:full_update', (syncmap_name, newSyncmap) => {
  const localSyncmap = cardtable[syncmap_name];
  for (let key in newSyncmap) {
    Vue.set(localSyncmap, key, newSyncmap[key]);
  }
  for (let key in localSyncmap) {
    if (!_.has(newSyncmap, key)) {
      Vue.delete(localSyncmap, key);
    }
  }
  if (syncmap_name === 'users') {
    if (!_.has(cardtable.users, my_name)) {
      setTimeout(() => {
        if (!_.has(cardtable.users, my_name)) {
          window.location.href = '/';
        }
      }, 500);
    }
  }
});

socket.on('syncmap:partial_update', (syncmap_name, newSyncmap) => {
  // TODO: finish this!
  const localSyncmap = cardtable[syncmap_name];
  for (let key in newSyncmap) {
    Vue.set(localSyncmap, key, newSyncmap[key]);
    if (newSyncmap[key] === null) {
      Vue.delete(localSyncmap, key);
    }
  }
  if (syncmap_name === 'users') {
    if (!_.has(cardtable.users, my_name)) {
      setTimeout(() => {
        if (!_.has(cardtable.users, my_name)) {
          window.location.href = '/';
        }
      }, 500);
    }
  }
});



// Dealer functions

//utils
function product(arr1, arr2) { return _.flatMap(arr1, r => _.map(arr2, s => r+s)); }
function shuffle(arr) {
  for (let i = arr.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [arr[i], arr[j]] = [arr[j], arr[i]];
  }
  return arr;
}
function getNPlayers(n) { // TODO: choose the players with the most armroom
  let names = Object.keys(cardtable.users).filter(name => name !== 'nobody');
  return shuffle(names).slice(0, n ? n : undefined);
}
function label2ID(label) {
  for (let cardID in cardtable.cards) {
    if (cardtable.cards[cardID].label === label) { return cardID; }
  }
  return null;
}
function nOfEach(n, arr, func) {
  if (!func) { func = _.identity; }
  return _.range(n).flatMap(idx => _.map(arr, elem => func(elem, idx)));
}
function getCards(filter) {
  let cardlist = _.map(cardtable.cards, (v,k) => ({cardID:k,label:v.label,pos:v.pos}));
  if (typeof filter === 'object') {
    cardlist = cardlist.filter(c => _.every(filter, (v,k) => c.pos[k] === v));
  }
  return cardlist;
}
function distSquared(pos1, pos2) {
  if (typeof pos2 === 'undefined') { pos2 = {x:.5, y:.5}; }
  if (pos1.type === 'hand' || pos2.type === 'hand') { return 1e100; }
  if ([pos1.x, pos1.y, pos2.x, pos2.y].some(x => typeof x !== 'number')) { return 1e100;}
  return Math.pow(pos1.x - pos2.x, 2) + Math.pow(pos1.y - pos2.y, 2);
}
function dispCard(c) { return `${c.label}:${c.pos.x.toFixed(2)},${c.pos.x.toFixed(2)}`; }
function clock2Rot(clock) { return ((clock + 6) % 12) / 12; }

// modifiers
function clearCards() { // TODO: only one network request (later, autobatch mods in 5ms)
  _.each(cardtable.cards, (v,k) => cardtable.deleteCard(k));
}
function placeCard(label, pos, addJitter) {
  if (/^X[hcds]$/.test(label)) { label = '10'+label[1]; }
  if (typeof pos === 'string') { pos = { type:'hand', user:pos }; }
  else if (typeof pos.x === 'number') { pos.type = 'table'; }
  if (addJitter && pos.type === 'table') {
    setDefaults(pos, {x:.5, y:.5, rot:0});
    pos.x += _.random(-.01, .01);
    pos.y += _.random(-.01, .01);
    pos.rot += _.random(-.01, .01);
  }
  cardtable.addCard(label, pos);
}
function flipLabel(label) {
  const cardID = label2ID(label);
  if (cardID) {
    cardtable.updateCardPos(cardID, {faceup: !cardtable.cards[cardID].pos.faceup});
  }
}
function pileUpLabels(labels, x,y, topTrump) {
  const rot = cardtable.users[my_name].rot;
  const [rotX, rotY] = rotate_xy(x,y, -rot)
  while (labels.length > (topTrump?1:0)) { placeCard(labels.pop(), {x:rotX, y:rotY, rot:-rot, faceup:false}, true); }
  if (topTrump && labels.length) { placeCard(labels.pop(), {x:rotX, y:rotY, rot:-rot, faceup:true}, true); }
}
function moveCard(card, pos, addJitter) {
  if (typeof pos === 'string') { pos = { type:'hand', user:pos }; }
  else if (Array.isArray(pos)) { pos = { type:'table', x:pos[0], y:pos[1] }; }
  else if (typeof pos.x === 'number') { pos.type = 'table'; }
  if (addJitter && _.every(['x','y','rot'], k => typeof pos[k]==='number')) {
    pos.x += _.random(-.01, .01);
    pos.y += _.random(-.01, .01);
    pos.rot += _.random(-.01, .01);
  }
  cardtable.updateCardPos(card.cardID, pos);
}

// bigger
function dealEuchre() {
  clearCards();
  const labels = shuffle(product('9XJQKA', 'hcds'));
  nOfEach(5, getNPlayers(4), name => { if (labels.length) placeCard(labels.pop(), name) });
  pileUpLabels(labels, .28,.75, true);
}
function dealPitch() {
  clearCards();
  const labels = shuffle(product('23456789XJQKA', 'hcds'));
  nOfEach(6, getNPlayers(4), name => { if (labels.length) placeCard(labels.pop(), name) });
  pileUpLabels(labels, .28,.75, true);
}
function dealWizard(handsize) {
  clearCards();
  const labels = shuffle(product('23456789XJQKA', 'hcds').concat(nOfEach(4, ['Wiz', 'je'])));
  nOfEach(handsize, getNPlayers(), name => { if (labels.length) placeCard(labels.pop(), name) });
  pileUpLabels(labels, .28,.75, labels.length>0)
}
function dealCanadianSalad() {
  clearCards();
  const labels = product('23456789XJQKA', 'hcds');
  const playerNames = getNPlayers();
  const numCardsPerPlayer = Math.floor(labels.length / playerNames.length);
  while (labels.length > numCardsPerPlayer * playerNames.length) { pileUpLabels([labels.shift()], .28,.75,true); }
  shuffle(labels);
  nOfEach(numCardsPerPlayer, playerNames, name => placeCard(labels.pop(), name));
}
function dealBatman() {
  clearCards();
  const labels = shuffle([].concat(
    nOfEach(5, ['1^bat']),
    nOfEach(2, ['2^Cat', '3^Ban', '4^Rob', '5^Poi']),
    ['6^2Fa', '7^Har', '8^Jok']
  ));
  getNPlayers().forEach(name => {if (labels.length) placeCard(labels.pop(), name)});
  pileUpLabels(labels, .5,.5, false);
}
function dealOhHell(handsize) {
  clearCards();
  const labels = shuffle(product('23456789XJQKA', 'hcds'));
  nOfEach(handsize, getNPlayers(), name => { if (labels.length) placeCard(labels.pop(), {type:'hand', user:name, faceup:handsize!==1})});
  pileUpLabels(labels, .28,.75, true);
}
function dealGolf(num_cards) {
  clearCards();
  const labels = shuffle(product('23456789XJQKA', 'hcds').concat(nOfEach(4, ['Jokr'])));
  nOfEach(num_cards, getNPlayers(), (name,idx) => {
    if (labels.length) {
      const rot = cardtable.users[name].rot;
      const x = .5 + .09* (idx % (num_cards/2) - num_cards/4 + 0.5);
      const y = .75 + .13*(idx >= num_cards/2);
      const [rotX, rotY] = rotate_xy(x,y, -rot);
      placeCard(labels.pop(), {x:rotX, y:rotY, rot:-rot, faceup:false});
    }
  });
  placeCard(labels.pop(), {x:.55,y:.5});
  pileUpLabels(labels, .45,.5);
}

function collectTrickN(num_cards, destClock) {
  // Gather cards from the middle and place them at `desClock` o'clock relative to user
  const tableCards = getCards({type:'table',faceup:true});
  const centralCards = _.sortBy(tableCards, c => distSquared(c.pos)).slice(0,num_cards);
  const rotation = clock2Rot(destClock) - cardtable.users[my_name].rot;
  const [x,y] = rotate_xy(.5,.8, rotation);
  centralCards.forEach(c => moveCard(c, {x:x,y:y}, true));
}
function collectCentralTrick(destClock, destRadius) {
  // Gather cards from the middle and place them at `destClock` o'clock relative to user
  if (typeof destRadius === 'undefined') { destRadius = 0.3; }
  const tableCards = getCards({type:'table',faceup:true});
  const distSquaredLimit = distSquared({x:.5,y:.63});
  const centralCards = tableCards.filter(c => distSquared(c.pos) < distSquaredLimit);
  const rotation = clock2Rot(destClock) - cardtable.users[my_name].rot;
  const [x,y] = rotate_xy(.5,0.5+destRadius, rotation);
  centralCards.forEach(c => moveCard(c, {x:x,y:y}, true));
}

window.addEventListener('keypress', evt => {
  if ('wasd'.includes(evt.key)) {
    const clock = {w:12, a:9, d:3, s:6}[evt.key];
    collectCentralTrick(clock + _.random(-0.9, 0.9), _.random(0.25, 0.35));
  } else if (evt.key === '~') {
    cardtable.admin_mode = !cardtable.admin_mode;
  }
});
window.addEventListener('keydown', evt => {
  if ([37,38,39,40].includes(evt.keyCode)) {
    const clock = {38:12, 37:9, 39:3, 40:6}[evt.keyCode];
    collectCentralTrick(clock + _.random(-0.9, 0.9), _.random(0.25, 0.35));
    evt.preventDefault();
  }
});
